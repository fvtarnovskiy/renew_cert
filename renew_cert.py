import sys
import shutil
import subprocess

try:
    server=sys.argv[1];
except:
    raise Exception("[ERROR] Server not defined!")

destPath="/var/www/my.pro-data.tech"
sourcePath="root@{}".format(server) + ":/etc/letsencrypt/live/my.pro-data.tech/{cert,chain,fullchain,privkey}.pem"

#
# Backup current certificates
#
archName= destPath + "_backup"
print("Backuping certificates..")
try: 
    shutil.make_archive(archName, 'zip', destPath)
except:
    raise Exception("[ERROR] Error backuping current certificates!")

#
# Copy certificates
#
print("Copying certificates..")
try: 
    subprocess.run(["scp", sourcePath, destPath])
except:
    raise Exception("[ERROR] Error copying certificates!")
#
# Restart apache2
# 
print("Reloading apache2.service..")
try: 
    subprocess.call(["sudo", "systemctl", "reload", "apache2.service"])
except:
    raise Exception("[ERROR] Error realoading apache2.service!")
print("Done")